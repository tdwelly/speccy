# Speccy - A dynamic spectrum+template viewer


## Legal:

Copyright (2016-2021) Tom Dwelly 

> Speccy is free software: you can redistribute it and/or modify
> it under the terms of the GNU General Public License as published by
> the Free Software Foundation, either version 3 of the License, or
> (at your option) any later version.

> This program is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
> GNU General Public License for more details.

> You should have received a copy of the GNU General Public License
> along with this program.  If not, see <http://www.gnu.org/licenses/>.

## Description:
Speccy is a web tool used to make visual inspections of astronomical spectra.
Speccy is implemented in javascript.

## Usage:
Speccy contains no spectral data of its own, you have to point it at the URL 
of an appropriately formatted spectral file. e.g.

> http://www.mpe.mpg.de/~tdwelly/speccy/speccy.html?specfile={SPECFILE_URL} 

(substitute URL of a valid speccy file)

Note that modern cross-site scripting prevention measures in the
browser generally prevent one from loading a specfile from a webserver
that is different from the one hosting the speccy webapp.


## Controls:
* The cursor (arrow) keys move the template in redshift and normalisation
* To zoom into a region: click and hold the right mouse button at one corner
  of the region of interest, then drag the pointer to the opposite corner of
  the region where you then release the right button (visual feedback is not 
  yet available).
* A left-click on the spectrum displays the coordinates of the pointer 
  position at the time of the click.
    
## Contact:
Please send comments, complaints and suggestions to dwelly 'at' mpe.mpg.de    

    