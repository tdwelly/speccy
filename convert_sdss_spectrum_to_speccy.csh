#!/bin/tcsh -f

set TEMPDIR = /tmp/${USER}/convert_sdss_spectrum_to_speccy
mkdir -p $TEMPDIR
set TEMP1 = ${TEMPDIR}/temp1_${HOST}_$$

#convert_sdss_spectrum_to_speccy.csh 
#Converts a standard SDSS format spectrum file into a speccy-compatible file format
#See speccy for details of speccy format: http://www.mpe.mpg.de/~tdwelly/speccy/speccy.html

if ( $#argv < 1 ) then
  echo "Error, correct usage: ${0:t} SPEC1 [SPEC2] [SPEC3] ..."
  exit 1
endif

set I = 1
while ( $I <= $#argv ) 
  set INFILE = "${argv[$I]}"
  set OUTFILE = "${INFILE:t:r}.txt"
  echo "Converting $INFILE to speccy format -> $OUTFILE"

  set KEYWORD_EXT = "SPECOBJ"
  if ( `ftlist ${INFILE} H mode=q | grep -c $KEYWORD_EXT` == 0 ) then
    set KEYWORD_EXT = "SPALL"
    # some spec files lack extnames
    if ( `ftlist ${INFILE} H mode=q | grep -c $KEYWORD_EXT` == 0 ) then
      set KEYWORD_EXT = "2"
    endif
  endif

  # some spec files lack extnames
  set COADD_EXT = "COADD"
  if ( `ftlist ${INFILE} H mode=q | grep -c $COADD_EXT` == 0 ) then
    set COADD_EXT = "1"
  endif

  # First convert the table info into a set of keywords
  ftlist "${INFILE}[${KEYWORD_EXT}]" C mode=q | tail -n +4 | awk '//{print "KEYNAME",$2}' > $TEMP1
  ftlist "${INFILE}[${KEYWORD_EXT}]" T mode=q separator='|' rownum=no colheader=no >> $TEMP1

 # deal with vector columns properly
  awk --field-separator='|' '\
function trim_string(s) {\
  for(__m=1;__m<=length(s);__m++){if(substr(s,__m,1)!=" "){break;}};\
  for(__n=length(s);__n>0;__n--) {if(substr(s,__n,1)!=" "){break;}};\
  return(substr(s,__m,__n-__m+1))}\
$0!~/^KEYNAME/ {\
  n++;nkey=NF;\
  for(i=1;i<=nkey;i++){\
    n_i=sprintf("%d_%d",n,i);\
    keyval[n_i]=trim_string($i)}}\
$0~/^KEYNAME/ {\
  k++;split($0,a," ");keyname[k]=a[2]}\
END {for(i=1;i<=nkey;i++){\
  printf("# %-20s = ", keyname[i]);\
  for(m=1;m<=n;m++){m_i=sprintf("%d_%d",m,i); printf("%s ",keyval[m_i])};printf("\n")}}' $TEMP1 >  $OUTFILE

  rm $TEMP1

  # now convert the pixel data into a list of Lambda,Flux,Error,Sky,Model quintuples.
  # ftlist "${INFILE}[${COADD_EXT}][col A=10**loglam;B=flux;C=ivar**-0.5;D=sky;E=model]" T mode=q colheader=no rownum=no separator="" | awk '{printf("%.4f,%.8g,%.8g,%.8g,%.8g\n", $1,$2,$3,$4,$5)}' >> ${OUTFILE}
  # this version avoids probbelms when reading illegal keywords
  ftlist "${INFILE}[${COADD_EXT}]" T columns="loglam,flux,ivar,sky,model" mode=q colheader=no rownum=no separator="" | awk '{printf("%.4f,%.8g,%.8g,%.8g,%.8g\n", 10.**$1,$2,($3>0.0?1.0/sqrt($3):0.0),$4,$5)}' >> ${OUTFILE}

  @ I ++
end




exit




########

