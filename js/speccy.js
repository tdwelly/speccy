/*    
   @licstart  The following is the entire license notice for the 
   JavaScript code in this page.

   Copyright (C) 2015-2021  Tom Dwelly

   The JavaScript code in this page is free software: you can
   redistribute it and/or modify it under the terms of the GNU
   General Public License (GNU GPL) as published by the Free Software
   Foundation, either version 3 of the License, or (at your option)
   any later version.  The code is distributed WITHOUT ANY WARRANTY;
   without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

   As additional permission under GNU GPL version 3 section 7, you
   may distribute non-source (e.g., minimized or compacted) forms of
   that code without the copy of the GNU GPL normally required by
   section 4, provided you include this license notice and a URL
   through which recipients can access the Corresponding Source.   

   @licend  The above is the entire license notice
   for the JavaScript code in this page.
*/



var canvas = document.getElementById("myCanvas");
var ctx = canvas.getContext("2d", {'alpha': false});  
ctx.canvas.width  = Math.max(1000, window.innerWidth * 0.96);
ctx.canvas.height = Math.max(500, window.innerHeight * 0.6);

var specfile = "";
//var submit_button = 0;
var submit_button = 1;
//var submit_php = 'http://erosita-bhm.mpe.mpg.de/efeds_vi/receive_vi.php';
var submit_php = '../efeds_vi/receive_vi.php';   // TODO
var lambdaArray = [];
var fluxArray   = [];
var fluxerrArray= [];
var skyArray    = [];
var modelArray  = [];
var residualArray  = [];
var skyTransLambdaArray  = [];
var skyTransFluxArray  = [];
var smoothedArray = [];
var smoothedresidualArray = [];
var templatelambdaArray = [];
var templatefluxArray = [];
var templateLength = 0;
var templateMean = 0.0;
var fluxMean = 0.0;
//var skymult = 0.05;
var padx1 = 50;
var padx2 = 10;
var padx = padx1 + padx2;
var pady = 50;
var mode = "SDSS4";
var specfile_slug = "";
//var xsize = canvas.width;
//var ysize = canvas.height; 


////Add a placeholder function for browsers that don't have setLineDash()
//if (!ctx.setLineDash) {
//    ctx.setLineDash = function () {}
//}

//var zorig = -99.9;
var modelz0 =  -99.9;           //the redshift that the model template has already been redshifted by
var modelz0_zpipe = -99.9;   //pipe
var znow = -99.9;       // the redshift that we want to display right now
var dznow_fine    = 1.0002;      //the factors in dznow that you get by pressing left or right mouse buttons
var dznow_coarse  = 1.005;       //
var dznow_vcoarse = 1.05;       //
var renorm = 1.0;
var drenorm = 1.02;

//these params control the wavelength+flux range plotted
var x0=1e10; //will derive values of these params from spectrum 
var x1=0.0;//
var y0=0.0;  //
var y1=0.0;  //
var orig_x0;
var orig_x1;
var orig_y0;
var orig_y1;
var prev_x0;
var prev_x1;
var prev_y0;
var prev_y1;

var stamp_numpix=80;
const stamp_scale_default=0.2;
var stamp_scale=stamp_scale_default;

var mousePos_old;
var leftclick = 0, middleclick = 1, rightclick = 2;
var nkeys = 0;
var keynames = [];
var keyvals = [];

// I have now made sure that all these lines are given with _vacuum_ wavelengths
// line wavelengths are mainly from here:
// "msw"       http://www.star.ucl.ac.uk/~msw/lines.html
// "B12"       Bolton et al., 2012, http://arxiv.org/abs/1207.7326v1
// "spZ"       http://data.sdss3.org/datamodel/files/BOSS_SPECTRO_REDUX/RUN2D/PLATE4/RUN1D/spZline.html
// "Charlton"  http://ned.ipac.caltech.edu/level5/Charlton/Charlton1_1.html
// "NIST"      http://physics.nist.gov/PhysRefData/ASD/lines_form.html (then converted to vacuum wavelengths)
// "NIST_Fe"   http://physics.nist.gov/PhysRefData/Handbook/Tables/irontable2.htm
// VB01        Vanden Berk et al (2001) http://adsabs.harvard.edu/abs/2001AJ....122..549V
var linelist = [];    //common emission and absorption lines
linelist.push(  {name:"Ly limit   "          , lambda:  912.    , emmabs:"Abs." , ref: "Charlton"  }); // 
linelist.push(  {name:"Ly\u03B3   "          , lambda:  972.537 , emmabs:"Em."  , ref: "Charlton"  }); // 
linelist.push(  {name:"Ly\u03B2   "          , lambda: 1025.722 , emmabs:"Em."  , ref: "Charlton"  }); // 
linelist.push(  {name:"Ly\u03B1   "          , lambda: 1215.67  , emmabs:"Em."  , ref: "spZ"	   }); // 
linelist.push(  {name:"NV         "          , lambda: 1240.81  , emmabs:"Em."  , ref: "spZ"	   }); // 
linelist.push(  {name:"SiIV       "          , lambda: 1393.755 , emmabs:"Em."  , ref: "Charlton"  }); // 
linelist.push(  {name:"SiIV       "          , lambda: 1402.770 , emmabs:"Em."  , ref: "Charlton"  }); // 
linelist.push(  {name:"CIV        "          , lambda: 1549.48  , emmabs:"Em."  , ref: "spZ"	   }); // 
linelist.push(  {name:"HeII       "          , lambda: 1640.42  , emmabs:"Em."  , ref: "spZ"	   }); // 
linelist.push(  {name:"CIII]      "          , lambda: 1908.734 , emmabs:"Em."  , ref: "spZ"	   }); // 
//linelist.push({name:"FeII       "          , lambda: 2382.765 , emmabs:"Em."  , ref: "Charlton"  }); // 
//linelist.push({name:"FeII       "          , lambda: 2600.173 , emmabs:"Em."  , ref: "Charlton"  }); // 
//linelist.push({name:"MgII       "          , lambda: 2800.3152, emmabs:"Em."  , ref: "spZ"	   }); // 
linelist.push(  {name:"MgII       "          , lambda: 2796.3522, emmabs:"Em."  , ref: "NIST"	   }); // 
linelist.push(  {name:"MgII       "          , lambda: 2803.5300, emmabs:"Em."  , ref: "NIST"	   }); // 
linelist.push(  {name:"[NeV]      "          , lambda: 3426.852 , emmabs:"Em."  , ref: "NIST"	   }); // 
linelist.push(  {name:"[OII]      "          , lambda: 3727.0917, emmabs:"Em."  , ref: "spZ"	   }); // 
linelist.push(  {name:"[OII]      "          , lambda: 3729.8754, emmabs:"Em."  , ref: "spZ"	   }); // 
linelist.push(  {name:"H\u03B7    "          , lambda: 3835.00  , emmabs:"Em."  , ref: "none"      }); // Heta, from wikipedia
linelist.push(  {name:"[NeIII]    "          , lambda: 3869.85  , emmabs:"Em."  , ref: "VB01"	   }); // 
linelist.push(  {name:"H\u03B6    "          , lambda: 3890.15  , emmabs:"Em."  , ref: "VB01"      }); // Hzeta (H8)
linelist.push(  {name:"CaII K     "          , lambda: 3934.777 , emmabs:"Abs." , ref: "msw"	   }); // 
//linelist.push({name:"[NeIII]  "            , lambda: 3968.58  , emmabs:"Em."  , ref: "VB01"      }); // deliberately excluded
linelist.push(  {name:"CaII H     "          , lambda: 3969.591 , emmabs:"Abs." , ref: "msw"	   }); // 
//linelist.push({name:"            H\u03B5"  , lambda: 3971.20  , emmabs:"Em."  , ref: "VB01"      }); // Hepsilon
linelist.push(  {name:"H\u03B4    "          , lambda: 4102.89  , emmabs:"Em."  , ref: "VB01"      }); // Hdelta
linelist.push(  {name:"G-band     "          , lambda: 4305.2   , emmabs:"Abs." , ref: "none"      }); // - converted from lambda_air=4304A 
linelist.push(  {name:"H\u03B3    "          , lambda: 4341.68  , emmabs:"Em."  , ref: "VB01"      }); // Hgamma
linelist.push(  {name:"     [OIII]"          , lambda: 4364.44  , emmabs:"Em."  , ref: "VB01"	   }); // 
linelist.push(  {name:"HeII       "          , lambda: 4686.9915, emmabs:"Em."  , ref: "spZ"	   }); // 
linelist.push(  {name:"H\u03B2    "          , lambda: 4862.68  , emmabs:"Em."  , ref: "VB01"      }); // Hbeta
linelist.push(  {name:"[OIII]     "          , lambda: 4960.30  , emmabs:"Em."  , ref: "VB01"	   }); // 
linelist.push(  {name:"[OIII]     "          , lambda: 5008.24  , emmabs:"Em."  , ref: "VB01"	   }); // 
linelist.push(  {name:"Mgb        "          , lambda: 5168.761 , emmabs:"Abs." , ref: "msw"	   }); // 
linelist.push(  {name:"Mgb        "          , lambda: 5174.125 , emmabs:"Abs." , ref: "msw" 	   }); // 
linelist.push(  {name:"Mgb        "          , lambda: 5185.048 , emmabs:"Abs." , ref: "msw" 	   }); // 
linelist.push(  {name:"NI         "          , lambda: 5200.53  , emmabs:"Em."  , ref: "VB01"      }); // narrow doublet 
//linelist.push({name:"FeII       "          , lambda: 5271.50  , emmabs:"Em."  , ref: "NIST"      }); // strongest one of a blend
linelist.push(  {name:"[OI]       "          , lambda: 5578.8878, emmabs:"Em."  , ref: "spZ"	   }); // 
linelist.push(  {name:"HeII       "          , lambda: 5413.0245, emmabs:"Em."  , ref: "spZ"	   }); // 
linelist.push(  {name:"[NII]      "          , lambda: 5756.1862, emmabs:"Em."  , ref: "spZ"	   }); // 
linelist.push(  {name:"HeI        "          , lambda: 5877.3086, emmabs:"Em."  , ref: "spZ"	   }); // 
linelist.push(  {name:"      NaD  "          , lambda: 5891.583 , emmabs:"Abs." , ref: "msw"	   }); // 
linelist.push(  {name:"      NaD  "          , lambda: 5897.558 , emmabs:"Abs." , ref: "msw"	   }); // 
linelist.push(  {name:"[OI]       "          , lambda: 6302.0464, emmabs:"Em."  , ref: "spZ"	   }); // 
linelist.push(  {name:"      [SIII]"         , lambda: 6313.8056, emmabs:"Em."  , ref: "spZ"	   }); // 
linelist.push(  {name:"[OI]       "          , lambda: 6363.776 , emmabs:"Em."  , ref: "spZ"	   }); // 
linelist.push(  {name:"      [NII]"          , lambda: 6549.8590, emmabs:"Em."  , ref: "spZ"	   }); // 
linelist.push(  {name:"H\u03B1    "          , lambda: 6564.6140, emmabs:"Em."  , ref: "spZ"       }); // Halpha
linelist.push(  {name:"      [NII]"          , lambda: 6585.2685, emmabs:"Em."  , ref: "spZ"	   }); // 
linelist.push(  {name:"[SII]      "          , lambda: 6718.2943, emmabs:"Em."  , ref: "spZ"	   }); // 
linelist.push(  {name:"[SII]      "          , lambda: 6732.6782, emmabs:"Em."  , ref: "spZ"	   }); // 
linelist.push(  {name:"[ArIII]    "          , lambda: 7137.7572, emmabs:"Em."  , ref: "spZ"	   }); // 
linelist.push(  {name:"[ArIII]    "          , lambda: 7753.2   , emmabs:"Em."  , ref: "msw"	   }); // 
linelist.push(  {name:"CaII       "          , lambda: 8500.36  , emmabs:"Abs." , ref: "msw"	   }); // 
linelist.push(  {name:"CaII       "          , lambda: 8544.44  , emmabs:"Abs." , ref: "msw"	   }); // 
linelist.push(  {name:"CaII       "          , lambda: 8664.52  , emmabs:"Abs." , ref: "msw"       }); //       

writeLineListTable(linelist);



canvas.oncontextmenu = function() {
    return false;  
} 

function interpSpecfileUrl (url){
    if ( url.match("sdssv_vi") ) {  // TODO make this more robust - switch on MJD?
        mode = "SDSS5";
    }
    // get the tail of the specfile filename and save it
    const temp = url.split("/");
    const temp2 = temp[temp.length - 1].split(".");
    //console.log(url);
    //console.log(temp2[0]);
    specfile_slug = temp2[0];
}
    
function updateFromMode( ) {
    if ( mode == "SDSS5" ) {
        var el = document.getElementById("th_plate");
        el.setAttribute("title", "SDSS-V Field ID");
        el.innerHTML = "Field";

        var el = document.getElementById("th_fiberid");
        el.setAttribute("title", "SDSS-V Catalogid");
        el.innerHTML = "Catalogid";

        var el = document.getElementById("th_target_bitmask");
        el.setAttribute("title", "SDSS-V SDSSV_BOSS_TARGET0 targeting bitmask");
        el.innerHTML = "SDSSV_BOSS_TARGET0";

        submit_php = '../sdssv_vi/receive_vi.php'; 
    }   
}



function startUpTasks()
{
    interpURL();
    if ( specfile != "" ) {
	readSpecFile();   //the get is asynchronous, so don't put anything after this that depends on the read
	changeTemplate();
    }
    readSkyTrans();
}


function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
	hash = hashes[i].split('=');
	vars.push(hash[0]);
	vars[hash[0]] = hash[1];
    }
    return vars;
}

function interpURL ()
{
    var ni = document.getElementById('baseDiv');
    var token_list = new getUrlVars();
    for (var i=0;i<token_list.length;i++){
	var par_name = token_list[i].toUpperCase();
	var par_val  = token_list[token_list[i]];
	var str = par_name + "=" + par_val;
	if ( par_name == "SPECFILE" ) {
	    specfile = par_val;
            var mode = interpSpecfileUrl(specfile); // this allows switching between SDSS-IV and -V views
            updateFromMode(mode);
	}
	else if ( par_name == "Z" ) {   //control the initial redshift that the model is plotted at
	    znow = Number(par_val);
	}
	else if ( par_name == "SUBMIT_BUTTON" ) {   //see if we should add a submit button
	    submit_button = Number(par_val);
	}	   
	else if ( par_name == "SUBMIT_PHP" ||  par_name == "SUBMIT_URL" ) {    //see which php page to submit the results to 
	    submit_php = par_val;
	}	   
    }
    if ( submit_button > 0 ) {
	document.getElementById("submitButton").type = "button";
    }
}

function AjaxError(x, e) {
    if (x.status == 0) {
	alert(' Check Your Network.');
    } else if (x.status == 404) {
	alert('Requested URL not found.');
    } else if (x.status == 500) {
	alert('Internal Server Error.');
    }  else {
	alert('Unknow Error.\n' + x.responseText);
    }
}


function readSpecFileFITS()
{
    alert('FITS files are not yet fully implemented!');
    
    //   var FITS = astro.FITS;
    // 
    //   // Define a callback function for when the FITS file is received
    //   var callback = function() {
    // 
    //     // Get the first header-dataunit containing a dataunit
    //     var hdu = this.getHDU();
    // 
    //     // or we can do
    //     var header = hdu.header;
    // 
    //     // Read a card from the header
    //     var bitpix = header.get('BITPIX');
    // 
    //     // Get the dataunit object
    //     var dataunit = hdu.data;
    // 
    //     // Do some wicked client side processing ...
    //     console.log("Read in fits file");
    //   }
    // 
    //   // Initialize a new FITS File object
    //   var fits = new FITS(specfile, callback);

}

function readSpecFileASCII()
{
    $.ajaxSetup({
	error: AjaxError
    });

    //read in the spec file
    var jQResult = jQuery.get(specfile, function(data) {
	var lines = [];
	lines = data.split("\n");
	var j = 0;
	var dy = 0.0;
	fluxMean = 0.0;
	nfluxMean = 0;

	//file format: lambda,flux,fluxerr,sky,model; 
	// lambda in AA; fluxes in units 1e-17 erg/cm/s/AA
	for(var i=0;i<lines.length;i++) 
	{
	    var data = [];
	    if ( lines[i].substr(0,1) == "#" ) {
		data  = (lines[i].substr(1)).split("=");
		if (data.length>=2 && (data[0].trim()).substr(0,1) != "#"){
		    keynames[nkeys] = (data[0].trim()).toUpperCase();
		    keyvals[nkeys]  = data[1].trim();
		    if ( keynames[nkeys] == "Z" ) {
			modelz0_zpipe = Number(keyvals[nkeys]);
			modelz0 = modelz0_zpipe;
		    }
		    else if ( keynames[nkeys] == "CLASS" ) 
		    {
			if (keyvals[nkeys].indexOf("QSO")  >= 0 ){
			    document.getElementById("class_person_qso").checked = 1;
			} 
			else if (keyvals[nkeys].indexOf("GALAXY")  >= 0 ) {
			    document.getElementById("class_person_galaxy").checked = 1;
			} 
			else if (keyvals[nkeys].indexOf("STAR")  >= 0 ) {
			    document.getElementById("class_person_star").checked = 1;
			}
		    }

		    nkeys ++;
		}
	    }
	    else
	    {
		data  = lines[i].split(",");
		if ( data.length >= 5 ) {
		    lambdaArray[j]  = Number(data[0]);
		    fluxArray[j]    = Number(data[1]);
		    fluxerrArray[j] = Number(data[2]);
		    skyArray[j]     = Number(data[3]);
		    modelArray[j]   = Number(data[4]);
		    residualArray[j] = Number(fluxArray[j] - modelArray[j]);

		    //calculate a clipped version of the mean of the flux 
                    if ( lambdaArray[j] >= 3800. && lambdaArray[j] <= 10000. )
                    {
                        if ( Math.abs(fluxerrArray[j] - modelArray[j]) < 1.0 * Math.abs(modelArray[j])) 
                        {   // the numeric value is the number of multiples of model flux to accept
                            if ( fluxArray[j] < y0 ) y0 = fluxArray[j];
                            if ( fluxArray[j] > y1 ) y1 = fluxArray[j];
                            if ( fluxArray[j] > -1e10) {
                                fluxMean = fluxMean + Number(fluxArray[j]); 
                                nfluxMean++;
                            }
                        }
                    }
		}
		j ++;
	    }
	};
	if (lambdaArray.length < 2) {
	    var str = "There was a problem with the specified specfile='"+specfile+"' (lambdaArray.length="+lambdaArray.length+")";
	    alert(str);
	}else{
	    if ( y1 <= y0 ) { y1 = y0 + 1.0; fluxMean = 0.5*(y0+y1);} //catch cases where no valid points
//            console.log(y0);
//            console.log(y1);
	    dy = (y1-Math.max(y0,0.0))*0.02; //expand the range a little
	    y1 = y1 + dy;
	    if(y0>=0.0) {
		y0 = y0 - dy;
	    }
	    else if ( y1 > 0.0 && Math.abs(y0) > 0.5 * y1 )
	    {
		y0 =  -0.5 * Math.abs(y1);
	    } 
	    x0 = Number(lambdaArray[0]) - 100;
	    x1 = Number(lambdaArray[(lambdaArray.length - 2)])+100.0;
	    orig_x0 = x0;
	    orig_x1 = x1;
	    orig_y0 = y0;
	    orig_y1 = y1;
	    prev_x0 = orig_x0;
	    prev_x1 = orig_x1;
	    prev_y0 = orig_y0;
	    prev_y1 = orig_y1;

	    fluxMean = fluxMean / (Math.max(nfluxMean,1));

	    if ( znow <= -99. ) {
		znow =  modelz0;
	    } 
	    var elem = document.getElementById("zinput");
	    elem.value = precise(znow, 7);

	    //calc the smoothed flux array
	    var boxsize =  $("input[type='radio'][name='smoothing']:checked").val();
	    smoothArray(fluxArray, smoothedArray, boxsize);
	    smoothArray(residualArray, smoothedresidualArray, boxsize);

	    plotData();
	    writePipeInfo();
            setupStamps(keyvals[keynames.indexOf("PLUG_RA")], keyvals[keynames.indexOf("PLUG_DEC")]);
	}
    });

}

function readSpecFile()
{
    var extension = specfile.split('.').pop();
//    console.log(extension);
    if ( extension === "fits" ) {
	readSpecFileFITS();
    } else {
	readSpecFileASCII();
    };
}


//things to do when the template is changed
//load new data
//replot
//switch on the value of the template select box
function changeTemplate() {
    var options = document.getElementById("templateSelect");
    var templateCode = document.getElementById("templateSelect").value; 
    var fluxcolnum;
    var templateURL = "not defined";
    if ( templateCode == "fromspec" ){
	templateURL = specfile; 
	fluxcolnum = 4;  
	modelz0 = modelz0_zpipe;
	for(var i=0;i<nkeys;i++) 
	{
	    if ( keynames[i] == "CLASS" ) {
		if (keyvals[i].indexOf("QSO")  >= 0 ){
		    document.getElementById("class_person_qso").checked = 1;
		} 
		else if (keyvals[i].indexOf("GALAXY")  >= 0 ) {
		    document.getElementById("class_person_galaxy").checked = 1;
		} 
		else if (keyvals[i].indexOf("STAR")  >= 0 ) {
		    document.getElementById("class_person_star").checked = 1;
		}
	    }
	}
	
    }
    else if ( templateCode == "fromURL" ){
	templateURL =  document.getElementById("templateURL").value;	    
	modelz0 = 0.0;
	fluxcolnum = 1;  
    }
    else if ( templateCode == "fromURLnoz" ){
	templateURL =  document.getElementById("templateURL").value;	    
	modelz0 = znow;
	fluxcolnum = 1;  
    }
    else
    {
	templateURL = "templates/template_"+templateCode+".txt"; 
	modelz0 = 0.0;
	fluxcolnum = 1;  
	var template_text = options[options.selectedIndex].text;
	if (template_text.indexOf("Star:") >= 0 )
	{
	    var elem = document.getElementById("zinput");
	    znow = 0.0;
	    elem.value = precise(znow, 1);
	    elem = document.getElementById("class_person_star");
	    elem.checked = 1;
	}
	else if (template_text.indexOf("Galaxy:") >= 0 )
	{
	    elem = document.getElementById("class_person_galaxy");
	    elem.checked = 1;
	} 
	else if (template_text.indexOf("QSO:") >= 0 )
	{
	    elem = document.getElementById("class_person_qso");
	    elem.checked = 1;
	}
    }

    $.ajaxSetup({
        error: AjaxError
    });

    //read in the file
    var jQResult = jQuery.get(templateURL, function(data) {
	var lines = [];
	lines = data.split("\n");
	//file format: lambda,flux
	templateLength = 0;
	var templateNmean = 0;
	templateMean = 0.0;
	for(var i=0;i<lines.length;i++) {
	    var data = [];
	    data  = lines[i].split(",");
	    templatelambdaArray[i] = Number(data[0]);
	    templatefluxArray[i]   = Number(data[fluxcolnum]);
	    if ( templatelambdaArray[i]*(1.+znow) >= orig_x0 && 
		 templatelambdaArray[i]*(1.+znow) <= orig_x1 )
	    {
		templateMean += templatefluxArray[i];
		templateNmean ++;
	    }
	    templateLength ++;
	};
	if (templateLength < 2) {
	    var str = "There was a problem with the specified template='"+templateURL+"'";
	    alert(str);
	}
	else {
	    if ( templateCode != "fromspec" && templateNmean > 0 )
	    {
		templateMean = templateMean / templateNmean;
		renorm = fluxMean / templateMean;
		//var str = "templateMean="+templateMean+" templateNmean="+templateNmean+" renorm="+renorm+" fluxMean="+fluxMean;
		//console.log(str);
	    }
	    else
	    {
		renorm = 1.0;
	    }
	    plotData();
	}
    });

}

function readSkyTrans() {
    const transURL = "templates/eso_skycalc_transmission_R3000.txt"; 
    //read in the file
    var jQResult = jQuery.get(transURL, function(data) {
	var lines = [];
	lines = data.split("\n");
	//file format: lambda(A),flux
	var templateLength = 0;
	for(var i=0;i<lines.length;i++) {
	    var data = [];
	    data  = lines[i].split(",");
	    skyTransLambdaArray[i] = Number(data[0]);
	    skyTransFluxArray[i]   = Number(data[1]);
	};
    })
}

function plotData() {
    ctx.canvas.width  = Math.max(1000, window.innerWidth * 0.96);
    ctx.canvas.height = Math.max(500, window.innerHeight * 0.5);
    var xsize = canvas.width;
    var ysize = canvas.height; 

    ctx.fillStyle = "white";
    ctx.rect(0, 0, canvas.width, canvas.height);
    ctx.fill();

    ctx.stroke();
    ctx.font = "14px Arial";
    ctx.textAlign="center"; 
    var zratio = (1. + znow)/(1. + modelz0);
    var dx_inv = (xsize-padx)/(x1-x0);
    var dy_inv = (ysize-2.*pady)/(y1-y0);
    var dx2_inv = (xsize-padx)*(1.+znow)/(x1-x0);
//    ctx.clearRect (0 ,0,canvas.width, canvas.height);


    ctx.fillStyle = "black";
    ctx.strokeStyle = "#000000";

    //determine the tics to use
    ctx.beginPath();
    var xtic_spacing;
    var ytic_spacing;
    if      (x1-x0 > 5000.0) { xtic_spacing = 1000; }
    else if (x1-x0 > 2500.0) { xtic_spacing = 500; }
    else if (x1-x0 > 1000.0) { xtic_spacing = 200; }
    else if (x1-x0 >  500.0) { xtic_spacing = 100; }
    else if (x1-x0 >  250.0) { xtic_spacing =  50; }
    else if (x1-x0 >  100.0) { xtic_spacing =  20; }
    else {xtic_spacing = 10;}
    var mxtic_spacing = 0.1*xtic_spacing;

    for(var xtic=0;xtic<=x1;xtic+=xtic_spacing){
	var x =         padx1 + (xtic-x0)*dx_inv;
	if ( x > padx1 && x < xsize-padx2){
	    ctx.fillText(xtic, x, ysize-0.6*pady);
	    ctx.moveTo(x,ysize-1.1*pady);
	    ctx.lineTo(x,ysize-1.2*pady);
	}
    }
    for(var xtic=0;xtic<=x1;xtic+=mxtic_spacing){
	var x =         padx1 + (xtic-x0)*dx_inv;
	if ( x > padx1 && x < xsize-padx2){
	    ctx.moveTo(x,ysize-1.0*pady);
	    ctx.lineTo(x,ysize-1.1*pady);
	}
    }

    ctx.stroke();
    ctx.closePath();

    ctx.beginPath();
    ctx.strokeStyle = "#aaaaaa";
    ctx.fillStyle = "#aaaaaa";
    // do the restframe wavelength tics 
    for(var xtic=0;xtic<=x1;xtic+=xtic_spacing){
	var x =         padx1 + (xtic*(1.+znow)-x0)*dx_inv;
	if ( x > padx1 && x < xsize-padx2){
            ctx.fillText(xtic, x, 0.8*pady);
            ctx.moveTo(x,1.1*pady);
            ctx.lineTo(x,1.2*pady);
	}
    }
    for(var xtic=0;xtic<=x1;xtic+=mxtic_spacing){
	var x =         padx1 + (xtic*(1.+znow)-x0)*dx_inv;
	if ( x > padx1 && x < xsize-padx2){
	    ctx.moveTo(x,1.0*pady);
	    ctx.lineTo(x,1.1*pady);
	}
    }
    var str_xlabel = "Restframe Vacuum Wavelength ("+ String.fromCharCode(8491)+")";
    ctx.fillText(str_xlabel, xsize*0.5, 0.3*pady);
    ctx.stroke();
    ctx.closePath();
    ctx.beginPath();
    ctx.strokeStyle = "#000000";
    ctx.fillStyle = "#000000";

    if      (y1-y0 > 50000.0) { ytic_spacing = 10000; }
    else if (y1-y0 > 25000.0) { ytic_spacing =  5000; }
    else if (y1-y0 > 10000.0) { ytic_spacing =  2000; }
    else if (y1-y0 >  5000.0) { ytic_spacing =  1000; }
    else if (y1-y0 >  2500.0) { ytic_spacing =   500; }
    else if (y1-y0 >  1000.0) { ytic_spacing =   200; }
    else if (y1-y0 >   500.0) { ytic_spacing =   100; }
    else if (y1-y0 >   250.0) { ytic_spacing =   50; }
    else if (y1-y0 >   100.0) { ytic_spacing =   20; }
    else if (y1-y0 >    50.0) { ytic_spacing =   10; }
    else if (y1-y0 >    25.0) { ytic_spacing =    5; }
    else if (y1-y0 >    10.0) { ytic_spacing =    2; }
    else if (y1-y0 >     5.0) { ytic_spacing =    1; }
    else {ytic_spacing = 0.5;}
    for(var ytic=0;ytic<=y1;ytic+=ytic_spacing){
	var y =  ysize - pady - (ytic-y0)*dy_inv;
	if ( y > pady && y < ysize-pady){
	    ctx.fillText(ytic, 0.6*padx1, y);
	    ctx.moveTo(padx1*1.1,y);
	    ctx.lineTo(padx1*1.2,y);
	}
    }
    var mytic_spacing = 0.2*ytic_spacing;


    for(var ytic=-ytic_spacing;ytic>=y0;ytic-=ytic_spacing){
	var y =  ysize - pady - (ytic-y0)*dy_inv;
	if ( y > pady && y < ysize-pady){
	    ctx.fillText(ytic, 0.6*padx1, y);
	    ctx.moveTo(padx1*1.1,y);
	    ctx.lineTo(padx1*1.2,y);
	}
    }

    for(var ytic=0;ytic<=y1;ytic+=mytic_spacing){
	var y =  ysize - pady - (ytic-y0)*dy_inv;
	if ( y > pady && y < ysize-pady){
	    ctx.moveTo(padx1,y);
	    ctx.lineTo(padx1*1.1,y);
	}
    }
    for(var ytic=-mytic_spacing;ytic>=y0;ytic-=mytic_spacing){
	var y =  ysize - pady - (ytic-y0)*dy_inv;
	if ( y > pady && y < ysize-pady){
	    ctx.moveTo(padx1,y);
	    ctx.lineTo(padx1*1.1,y);
	}
    }
    //draw horizontal line at flux=0
    {
	var y =  ysize - pady - (0.0-y0)*dy_inv;
	if ( y > pady && y < ysize-pady){
	    ctx.moveTo(padx1,y);
	    ctx.lineTo(xsize-padx2,y);
	}
    }
    ctx.stroke();
    ctx.closePath();

    //draw the axis labels
    ctx.beginPath();
    var str_xlabel = "Observed Vacuum Wavelength ("+ String.fromCharCode(8491)+")";
    ctx.fillText(str_xlabel, xsize*0.5, ysize-0.2*pady);
    ctx.save();
    ctx.translate(0,ysize);
    ctx.rotate(-Math.PI/2);
    ctx.textAlign = "center";
    //  ctx.fillText("Flux (1e-17 erg cm^-2 s^-1 AA^-1)",  padx1*0.2, ysize*0.5);
    var str_ylabel = "Flux (10"+String.fromCharCode(8315,185,8311)+" erg cm"+String.fromCharCode(8315,178) +" s"+ String.fromCharCode(8315,185) + " " + String.fromCharCode(8491,8315,185)+")";
    ctx.fillText(str_ylabel,  ysize*0.5, padx1*0.3);
    ctx.restore();
    ctx.stroke();
    ctx.closePath();

    // write the plate-mjd-fiberid in the top left hand corner
    {
        // var plate = parseInt(document.getElementById("pipeParams_PLATE").innerHTML);
        // var mjd = parseInt(document.getElementById("pipeParams_MJD").innerHTML);
        // var fiberid = parseInt(document.getElementById("pipeParams_FIBERID").innerHTML);
        var run2d = keyvals[keynames.indexOf('RUN2D')];
        // var run2d = document.getElementById("pipeParams_RUN2D").innerHTML;
        // var str_plate = (plate < 10000 ? ("0000" + plate).slice(-4) : ("00000" + plate).slice(-5));
        ctx.font = "18px Arial";
        ctx.textAlign = "left";
        ctx.beginPath();
        //var str_label = str_plate + "-" + mjd + "-" + ("0000" + fiberid).slice(-4) + " (" + run2d + ")";
        var str_label = specfile_slug.replace('spec-', '') + " (" + run2d + ")";
        ctx.fillText(str_label, 5.0+xsize*0.005, 10.0+ysize*0.01);
        ctx.stroke();
        ctx.closePath();
        ctx.font = "14px Arial";
        ctx.textAlign = "center";
    }
    
    //now draw wavelengths of common lines
    if ( document.getElementById("ToggleLines").checked )
    {  
	var last_name = "";
	var last_x    = -1.;
	ctx.strokeStyle = "#008800";
	ctx.fillStyle = "#008800";
	ctx.font = "12px Arial";
	ctx.textAlign="left"; 
	ctx.save();
	ctx.translate(0,ysize);
	ctx.rotate(-Math.PI/2);
	ctx.beginPath();
	
	for(var lineindex=0; lineindex < linelist.length; lineindex++)
	{
	    if ( linelist[lineindex].emmabs == "Em." )
	    {
		var lobs = (1.0+znow)*Number(linelist[lineindex].lambda);
		var x = padx1 + (lobs-x0)*dx_inv;
		if ( x > padx1 && x < xsize-padx2) {
		    if ( linelist[lineindex].name != last_name || 
			 x > (last_x + 8.))
		    {
			ctx.fillText(linelist[lineindex].name, ysize-0.95*pady, x+4.0);
		    }
		    ctx.moveTo(pady,x);
		    ctx.lineTo(ysize-pady,x);
		    last_name = linelist[lineindex].name;
		    last_x = x;
		}
	    }
	}
	ctx.stroke();
	ctx.closePath();

	//now draw the pure absorption lines
	ctx.beginPath();
	ctx.strokeStyle = "#888800";
	ctx.fillStyle = "#888800";
	for(var lineindex=0; lineindex < linelist.length; lineindex++)
	{
	    if ( linelist[lineindex].emmabs == "Abs." )
	    {
		var lobs = (1.0+znow)*Number(linelist[lineindex].lambda);
		var x = padx1 + (lobs-x0)*dx_inv;
		if ( x > padx1 && x < xsize-padx2) {
		    if ( linelist[lineindex].name != last_name || 
			 x > (last_x + 8.))
		    {
			ctx.fillText(linelist[lineindex].name, ysize-0.95*pady, x+4.0);
		    }
		    ctx.moveTo(pady,x);
		    ctx.lineTo(ysize-pady,x);
		    last_name = linelist[lineindex].name;
		    last_x = x;
		}
	    }
	}
	ctx.stroke();


	ctx.restore();
	ctx.closePath();
	ctx.fillStyle = "#000000";
	ctx.strokeStyle = "#000000";
	ctx.font = "14px Arial";
	ctx.textAlign="center"; 
    }


    //draw the sky curve
    if ( document.getElementById("ToggleSky").checked )
    {  
	var skymult = Number(document.getElementById("skyMult").value);
	ctx.strokeStyle = "#00dddd";
	ctx.beginPath();
	for(var i=0;i<lambdaArray.length;i++){
	    var x = padx1 + (lambdaArray[i]-x0)*dx_inv;
	    if ( x >= padx1 && x <= xsize-padx2)
	    {
		var y = ysize - pady - (skyArray[i]*skymult-y0)*dy_inv;
		var clip_y = Math.max(Math.min(y,ysize-pady),pady);
		if ( i>=1) { ctx.lineTo(x,clip_y);}
		else       { ctx.moveTo(x,clip_y);}
	    }
	}
	ctx.stroke();
	ctx.closePath();
    }


    //draw the observed flux curve
    if ( document.getElementById("ToggleFlux").checked )
    {  
	//    ctx.strokeStyle = "#0000dd";
	ctx.strokeStyle = "#000000";
	ctx.beginPath();
	for(var i=0;i<lambdaArray.length;i++){
	    var x =         padx1 + (lambdaArray[i]-x0)*dx_inv;
	    if ( x >= padx1 && x <= xsize-padx2 ) 
	    { 
		var y = ysize - pady - (smoothedArray[i]-y0)*dy_inv;
		var clip_y = Math.max(Math.min(y,ysize-pady),pady);
		if ( i>=1) { ctx.lineTo(x,clip_y);}
		else       { ctx.moveTo(x,clip_y);}
	    }
	}
	ctx.stroke();
	ctx.closePath();
    }

    //draw the residual flux curve
    if ( document.getElementById("ToggleResidual").checked )
    {  
	ctx.strokeStyle = "#ff8c00";
	ctx.beginPath();
	for(var i=0;i<lambdaArray.length;i++){
	    var x =         padx1 + (lambdaArray[i]-x0)*dx_inv;
	    if ( x >= padx1 && x <= xsize-padx2 ) 
	    { 
		var y = ysize - pady - (smoothedresidualArray[i]-y0)*dy_inv;
		var clip_y = Math.max(Math.min(y,ysize-pady),pady);
		if ( i>=1) { ctx.lineTo(x,clip_y);}
		else       { ctx.moveTo(x,clip_y);}
	    }
	}
	ctx.stroke();
	ctx.closePath();
    }
    
    //draw the sky transmission curve
    if ( document.getElementById("ToggleTrans").checked )
    {
        var skytransmult = Math.max(0.0,y0) + 0.5*(y1 -Math.max(0,y0));
        
	ctx.strokeStyle = "#0000aa";
	ctx.beginPath();
	for(var i=0;i<skyTransLambdaArray.length;i++){
	    var x = padx1 + (skyTransLambdaArray[i]-x0)*dx_inv;
	    if ( x >= padx1 && x <= xsize-padx2 ) 
	    { 
		var y = ysize - pady - (skyTransFluxArray[i]*skytransmult-y0)*dy_inv;
		var clip_y = Math.max(Math.min(y,ysize-pady),pady);
		if ( i>=1) { ctx.lineTo(x,clip_y);}
		else       { ctx.moveTo(x,clip_y);}
	    }
	}
	ctx.stroke();
	ctx.closePath();
    }

    //draw the model curve
    if ( document.getElementById("ToggleModel").checked )
    {  
	ctx.beginPath();
	ctx.strokeStyle = "#ff0000";
	for(var i=0;i<templateLength;i++){
	    var x =         padx1 + (templatelambdaArray[i]*zratio-x0)*dx_inv;
	    if ( x >= padx1 && x <= xsize-padx2 )
	    {
		var y = ysize - pady - (templatefluxArray[i]*renorm-y0)*dy_inv;
		var clip_y = Math.max(Math.min(y,ysize-pady),pady);
		if ( i>=1) { ctx.lineTo(x,clip_y);}
		else       { ctx.moveTo(x,clip_y);}
	    }
	}
	ctx.stroke();
	ctx.closePath();
    }

    //draw the noise curve
    if ( document.getElementById("ToggleNoise").checked )
    {  
	ctx.beginPath();
	ctx.strokeStyle = "#aaaaaa";
	for(var i=0;i<lambdaArray.length;i++){
	    var x =         padx1 + (lambdaArray[i]-x0)*dx_inv;
	    if ( x >= padx1 && x <= xsize-padx2 ) 
	    { 
		var y = ysize - pady - (fluxerrArray[i]-y0)*dy_inv;
		var clip_y = Math.max(Math.min(y,ysize-pady),pady);
		if ( i>=1) { ctx.lineTo(x,clip_y);}
		else       { ctx.moveTo(x,clip_y);}
	    }
	}
	ctx.stroke();
	ctx.closePath();
    }


    //draw the border
    ctx.strokeStyle = "#000000";
    ctx.beginPath();
    ctx.moveTo(padx1,pady);
    ctx.lineTo(padx1,ysize-pady);
    ctx.lineTo(xsize-padx2,ysize-pady);
    ctx.lineTo(xsize-padx2,pady);
    ctx.lineTo(padx1,pady);
    ctx.stroke();
    ctx.closePath();


}

function incrementRedshift(zfactor)
{
    var elem = document.getElementById("zinput");
    znow = (1. + znow)*zfactor - 1.0;
    //  elem.value = znow;
    elem.value = precise(znow, 7); //sprintf("%.7g",znow);
    plotData();
    writeVIinfo();
}
function decrementRedshift(zfactor)
{
    var elem = document.getElementById("zinput");
    znow = (1. + znow)/zfactor - 1.0;
    //  elem.value = znow;
    elem.value = precise(znow, 7); //sprintf("%.7g",znow);
    plotData();
    writeVIinfo();
}

function updateFromzinput(){
    var zinput = Number(document.getElementById("zinput").value); 
    znow = zinput;
    plotData();
    writeVIinfo();
}

function resetTozpipe()
{
    var elem = document.getElementById("zinput"); 
    znow = modelz0_zpipe;
    elem.value = precise(znow, 7);
    plotData();
    writeVIinfo();
}
function setTominus1()
{
    var elem = document.getElementById("zinput"); 
    znow = -1.0;
    elem.value = precise(znow, 1);
    plotData();
    writeVIinfo();
}

function setTozero()
{
    var elem = document.getElementById("zinput"); 
    znow = 0.0;
    elem.value = precise(znow, 1);
    plotData();
    writeVIinfo();
}


function updateSmoothing(){
    //re-calc the smoothed flux array
    var boxsize =  $("input[type='radio'][name='smoothing']:checked").val();
    smoothArray(fluxArray, smoothedArray, boxsize);
    smoothArray(residualArray, smoothedresidualArray, boxsize);
    plotData();
}


function resetZoom(){
    x0 = orig_x0;
    x1 = orig_x1;
    y0 = orig_y0;
    y1 = orig_y1;
    plotData();
}
function previousZoom(){
    x0 = prev_x0;
    x1 = prev_x1;
    y0 = prev_y0;
    y1 = prev_y1;
    plotData();
}

function applyZoom(xfactor,yfactor){
    prev_x0 = x0;
    prev_x1 = x1;
    prev_y0 = y0;
    prev_y1 = y1;
    x0 = 0.5*(prev_x0+prev_x1) - 0.5*(prev_x1-prev_x0)*xfactor;
    x1 = 0.5*(prev_x0+prev_x1) + 0.5*(prev_x1-prev_x0)*xfactor;
    y0 = 0.5*(prev_y0+prev_y1) - 0.5*(prev_y1-prev_y0)*yfactor;
    y1 = 0.5*(prev_y0+prev_y1) + 0.5*(prev_y1-prev_y0)*yfactor;
    plotData();
}
function applyOffset(xfrac,yfrac){
    prev_x0 = x0;
    prev_x1 = x1;
    prev_y0 = y0;
    prev_y1 = y1;
    var xmid = 0.5*(prev_x0+prev_x1) + (prev_x1-prev_x0)*xfrac;
    var ymid = 0.5*(prev_y0+prev_y1) + (prev_y1-prev_y0)*yfrac;
    x0 = xmid - 0.5*(prev_x1-prev_x0);
    x1 = xmid + 0.5*(prev_x1-prev_x0);
    y0 = ymid - 0.5*(prev_y1-prev_y0);
    y1 = ymid + 0.5*(prev_y1-prev_y0);
    plotData();
}

function check_valid_conf_class(z_conf_person, class_person){
    var result = false;
//    console.log(z_conf_person);
//    console.log(class_person);
//    console.log(typeof z_conf_person);
//    console.log(typeof class_person);
    if ( (z_conf_person == 0 && class_person == "NONE") ||
         (z_conf_person == 1 && class_person == "NONE") ||
         (z_conf_person == 1 && class_person == "QSO" )) { result = true; }
    else if ( (z_conf_person >= 2 && z_conf_person <= 3) &&
              (class_person == "NONE" ||
               class_person == "STAR" ||
               class_person == "QSO" ||
               class_person == "GALAXY" ||
               class_person == "QSO_BAL" ||
               class_person == "BLAZAR") ) { result = true; }

    return result;
}

//see guide at https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/Forms/Sending_forms_through_JavaScript
//write required values to url encoded string and send using an XMLHttpRequest
function submitVIinfo(){
    var XHR = new XMLHttpRequest();
    var FD  = new FormData();

    //gather the data
    var plate = "-1";
    var mjd = "-";
    var fiberid = "-1";
    var ra = "-99.9";
    var dec = "-99.9";
    var z_conf_person = $("input[type='radio'][name='z_conf_person']:checked").val();
    var class_person  = $("input[type='radio'][name='class_person']:checked").val();
    var issue         = document.getElementById("issueSelect").value; 
    var comments      = document.getElementById("commentBox").value; 
    var is_blagn       = $('#isBlagn').is(":checked"); 
    var is_interesting = $('#isInteresting').is(":checked"); 
    var run1d = "-";
    var run2d = "-";
    var field = '-1';   // new for SDSS-V
    var designid = '-1';
    var catalogid = '-1';
    for(var i=0;i<nkeys;i++) 
    {
	switch (keynames[i]) {
	    case "PLATE":    plate   = keyvals[i]; break;
	    case "MJD":      mjd     = keyvals[i]; break;
	    case "FIBERID":  fiberid = keyvals[i]; break;
	    case "PLUG_RA":  ra      = keyvals[i]; break;
	    case "PLUG_DEC": dec     = keyvals[i]; break;
	    case "Z":        zpipe   = keyvals[i]; break;
	    case "RUN1D":    run1d   = keyvals[i]; break;
	    case "RUN2D":    run2d   = keyvals[i]; break;
            case "CATALOGID": catalogid = keyvals[i]; break;
            case "FIELD":    field   = keyvals[i]; break;
            case "DESIGNID": designid = keyvals[i]; break;

	}
    }

    // catch invalid combinations of class/z_conf
    if ( !check_valid_conf_class(Number(z_conf_person), class_person) )
    {
        alert('Error! You supplied an illegal combination of Z_CONF_PERSON and CLASS_PERSON.');
    }
    
    FD.append("PLATE"        ,plate);
    FD.append("MJD"          ,mjd);
    FD.append("FIBERID"      ,fiberid);
    FD.append("RA"           ,ra);
    FD.append("DEC"          ,dec);
    FD.append("Z_PIPE"       ,zpipe);
    FD.append("Z_VI"         ,znow);
    FD.append("Z_CONF_PERSON",z_conf_person);
    FD.append("CLASS_PERSON" ,class_person);
    FD.append("ISSUE"        ,issue);
    FD.append("COMMENTS"     ,comments);
    FD.append("RUN1D"        ,run1d);
    FD.append("RUN2D"        ,run2d);
    FD.append("ISINTERESTING",is_interesting);
    FD.append("ISBLAGN"      ,is_blagn);
    FD.append("CATALOGID"    ,catalogid);
    FD.append("FIELD"        ,field);
    FD.append("DESIGNID"     ,designid);


    //   // -----------------------------
    //   // The following doesn't seem to do anything....maybe it catches errors that I've not tested against? 
    //   // We define what will happen if the data are successfully sent
    //   XHR.addEventListener('load', function(event) { 
    //     //    alert('Success! Data sent and response loaded.');
    //     //add a pip to the submission button
    //     document.getElementById("submitButton").value += ".";
    //   });
    // 
    //   // We define what will happen in case of error
    //   XHR.addEventListener('error', function(event) {
    //     alert('Error! Something went wrong when submitting your results.');
    //   });
    //   // -----------------------------

    // We setup our request ('false' flag specifies that this should be synchronous)
    XHR.open('POST', submit_php, false);

    // We just send our FormData object, HTTP headers are set automatically
    XHR.send(FD);

    // This catches some errors
    if (XHR.status != 0) {
	var success_text = "Success!"; 
	if (XHR.responseText.substring(0, success_text.length) === success_text) {
	    //add a pip to the submission button
	    button = document.getElementById("submitButton");
            button.value += ".";
            button.style.background = "rgba(128,128,128,0.5)";  //grey out the button
	} else {
	    alert('Error! Something went badly wrong when submitting your results (see console for details).');
	    console.log(XHR.responseText); //debug output for those who are interested
	};
    };
}


//write formatted description of the current fit
//requires that the header keywords are in the input spec file and stored in the keynames[],keyvals[] arrays
function writeVIinfo(){
    var outbox = document.getElementById("resultBox"); 
    var str = "";
    var z_conf_person = $("input[type='radio'][name='z_conf_person']:checked").val();
    var class_person  = $("input[type='radio'][name='class_person']:checked").val();
    var issue         = document.getElementById("issueSelect").value; 
    var comments      = document.getElementById("commentBox").value; 
    var is_blagn       = $('#isBlagn').is(":checked"); 
    var is_interesting = $('#isInteresting').is(":checked"); 
    var str_is_blagn = "";
    var str_is_interesting = "";
    var plate     = keyvals[keynames.indexOf("PLATE")] || 'NaN';
    var field     = keyvals[keynames.indexOf("FIELD")] || 'NaN';
    var mjd       = keyvals[keynames.indexOf("MJD")] || 'NaN';
    var fiberid   = keyvals[keynames.indexOf("FIBERID")] || 'NaN';
    var ra        = keyvals[keynames.indexOf("PLUG_RA")] || 'NaN';
    var dec       = keyvals[keynames.indexOf("PLUG_DEC")] || 'NaN';
    var zpipe     = keyvals[keynames.indexOf("Z")] || 'NaN';
    var catalogid = keyvals[keynames.indexOf("CATALOGID")] || 'NaN';
    var field     = keyvals[keynames.indexOf("FIELD")] || 'NaN'; 
    var designid  = keyvals[keynames.indexOf("DESIGNID")] || 'NaN';
    
    if ( is_blagn == true ) { str_is_blagn = " BLAGN!";}
    if ( is_interesting == true ) { str_is_interesting = " Interesting!";}
    str = sprintf("SDSSOBJECT %5d %5d %4d %12.7f %12.7f %10s %.7g %3s \"%8s\" \"%s\" \"%s\" %011d %d %d %s%s\n", 
		  plate, mjd, fiberid, ra, dec, zpipe, znow, z_conf_person,
		  class_person, issue, comments, 
                  catalogid, field, designid,
                  str_is_blagn, str_is_interesting);
    outbox.value = str;
}

function precise(val, n=5) {
    return Number.parseFloat(val).toPrecision(n);
}

function writePipeInfo(){
    var ra = Number.parseFloat(keyvals[keynames.indexOf("PLUG_RA")]) || 'NaN';
    var dec = Number.parseFloat(keyvals[keynames.indexOf("PLUG_DEC")]) || 'NaN'; 

    var el = document.getElementById("pipeParams_PLATE");
    if (mode == "SDSS4") { el.innerHTML = keyvals[keynames.indexOf("PLATE")] || 'NaN';}
    if (mode == "SDSS5") { el.innerHTML = keyvals[keynames.indexOf("FIELD")] || 'NaN';}

    var el = document.getElementById("pipeParams_FIBERID");
    if (mode == "SDSS4") { el.innerHTML = keyvals[keynames.indexOf("FIBERID")] || 'NaN';}
    if (mode == "SDSS5") { el.innerHTML = keyvals[keynames.indexOf("CATALOGID")] || 'NaN';}

    document.getElementById("pipeParams_RUN2D").innerHTML = keyvals[keynames.indexOf("RUN2D")] || 'NaN';
    document.getElementById("pipeParams_PLUG_RA").innerHTML = ra.toFixed(7);
    document.getElementById("pipeParams_PLUG_DEC").innerHTML = dec.toFixed(6);
    document.getElementById("pipeParams_Z").innerHTML = precise(keyvals[keynames.indexOf("Z")],7) || 'NaN';
    document.getElementById("pipeParams_Z_ERR").innerHTML = precise(keyvals[keynames.indexOf("Z_ERR")],3) || 'NaN';
    document.getElementById("pipeParams_CLASS").innerHTML = keyvals[keynames.indexOf("CLASS")] || 'NaN';
    document.getElementById("pipeParams_SUBCLASS").innerHTML = keyvals[keynames.indexOf("SUBCLASS")] || '';
    document.getElementById("pipeParams_SN_MEDIAN_ALL").innerHTML = precise(keyvals[keynames.indexOf("SN_MEDIAN_ALL")],4) || 'NaN';

    var el = document.getElementById("pipeParams_MJD");
    var mjd = keyvals[keynames.indexOf("MJD")] || 'NaN';
    el.innerHTML = mjd;
    el.title = convertMJDtoDate(mjd).toISOString().split('T')[0];
    var zwarning = keyvals[keynames.indexOf("ZWARNING")] || 'NaN';
    document.getElementById("pipeParams_ZWARNING").innerHTML =
    sprintf("<a href='../bitmask_decoder/sdss_bitmask_decoder.html?ZWARNING=%s' target=_blank title='decode the ZWARNING flag'>%s</a>", zwarning, zwarning);

    var el = document.getElementById("pipeParams_TARGET_BITMASK");
    if (mode == "SDSS4") {
        var bitmask = keyvals[keynames.indexOf("EBOSS_TARGET1")] || 'NaN';
        el.innerHTML = sprintf("<a href='../bitmask_decoder/sdss_bitmask_decoder.html?EBOSS_TARGET1=%s' target=_blank title='decode the EBOSS_TARGET1 flag'>%s</a>", 
			       bitmask, bitmask);
    };
    if (mode == "SDSS5") {
        var bitmask = keyvals[keynames.indexOf("SDSSV_BOSS_TARGET0")] || 'NaN';
        el.innerHTML = sprintf("<a href='../bitmask_decoder/sdss_bitmask_decoder.html?SDSSV_BOSS_TARGET0=%s' target=_blank title='decode the SDSSV_BOSS_TARGET0 flag'>%s</a>", 
			       bitmask, bitmask);
    };

    
    var str = "";
    if ( ra > -99. && dec > -99.) {
	var str1 = sprintf("<a href='http://skyserver.sdss.org/dr16/en/tools/explore/summary.aspx?ra=%g&dec=%g' target='_blank' title='View imaging for this object in the SDSS Explore service'>Explore</a>", ra, dec); 
	var str2 = sprintf("<a href='http://simbad.u-strasbg.fr/simbad/sim-coo?output.format=HTML&Coord=%s+%s&Radius=10&Radius.unit=arcsec' target='_blank' title='Search around this position in the Simbad service'>Simbad</a>", ra, dec); 
	var str3 = sprintf("<a href='http://legacysurvey.org/viewer/?ra=%g&dec=%g&zoom=16&layer=ls-dr9&mark=%g,%g' target='_blank' title='View optical imaging for this object on the legacysurvey.org site'>ls-dr9</a>", ra, dec, ra, dec); 
	var str4 = sprintf("<a href='http://legacysurvey.org/viewer/?ra=%g&dec=%g&zoom=16&layer=unwise-neo6&mark=%g,%g' target='_blank' title='View unWISE imaging for this object on the legacysurvey.org site'>unWISE</a>", ra, dec, ra, dec); 
        var str5 = sprintf("<a href='http://vizier.u-strasbg.fr/viz-bin/VizieR-5?-ref=VIZ60fed5911f70ac&-out.add=.&-source=I/350/gaiaedr3&-c=%.8g%20%s%.8g,eq=ICRS,rs=3&-out.orig=o' target='_blank' title='Search for nearest Gaia EDR3 catalogue entry to this location (within 3arcsec)'>GaiaEDR3</a>", ra, (dec<0.0?"":"+"),dec); 
	str = str1.concat(" ",str2," ",str3," ",str4, " ", str5); 
    } 
    document.getElementById("pipeParams_links").innerHTML = str;

    str = sprintf("Inspect the original specfile in your browser: <a href='%s' target='_blank' title='Inspect the original specfile in your browser' >%s</a>", specfile, specfile);
    document.getElementById("inspectInBrowser").innerHTML = str;
}



function smoothArray(original, smoothed, boxsize)
{
    if ( boxsize == 1 ) 
    {
	for (var i=0; i < original.length; i++) smoothed[i] = original[i];
    }
    else if (  boxsize > 1 )
    {
	var halfbox = Math.floor(boxsize/2);
	for (var i=0; i < original.length; i++)
	{
	    var n   = 0;
	    var sum = 0.0;
	    for (var j=i-halfbox; j <= i+halfbox; j++)
	    {
		if ( j >= 0 && j < original.length )
		{
		    sum += original[j];
		    n += 1;
		}
	    }
	    smoothed[i] = sum / ( n > 0 ? n : 1);
	}
    }
}

function scaleModel(factor){
    renorm *= factor;
    plotData();
}

//deal with key presses
canvas.addEventListener('keydown',doKeyDown,true);

function doKeyDown(evt){
    //  console.log(evt.keyCode);
    switch (evt.keyCode) {
	case 38:  /* Up arrow was pressed */
	    scaleModel(drenorm);
	    break;
	case 40:  /* Down arrow was pressed */
	    scaleModel(1./drenorm);
	    break;
	case 37:  /* Left arrow was pressed */ 
	    decrementRedshift(dznow_fine);
	    break;
	case 39:  /* Right arrow was pressed */
	    incrementRedshift(dznow_fine);
	    break;
    }
}




///deal with mouse zooming etc

function getMousePos(canvas, evt) {
    var rect = canvas.getBoundingClientRect();
    return {
	x: Math.round((evt.clientX-rect.left)/(rect.right-rect.left)*canvas.width),
	y: Math.round((evt.clientY-rect.top)/(rect.bottom-rect.top)*canvas.height)
    };
}

function getMousePosExact(canvas, evt) {
    var rect = canvas.getBoundingClientRect();
    return {
	x: ((evt.clientX-rect.left)/(rect.right-rect.left)*canvas.width),
	y: ((evt.clientY-rect.top)/(rect.bottom-rect.top)*canvas.height)
    };
}

//document.body.addEventListener('mousedown', function (evt){
canvas.addEventListener('mousedown', function (evt){
    if(evt.button === rightclick  ){
	//record the current cursor position;
	mousePos_old = getMousePos(canvas, evt);
    }
    else if(evt.button === leftclick )
    {
	//display the current position of the cursor
	var mousePos = getMousePosExact(canvas, evt);
	var xsize = canvas.width;
	var ysize = canvas.height; 
	if ( mousePos.x >= 0.0 && mousePos.x <= xsize && 
             mousePos.y >= 0.0 && mousePos.y <= ysize ) {
		 var dl_dx = (x1-x0)/(xsize-padx);
		 var dflux_dy = (y1-y0)/(ysize-2.*pady);
		 var lobs = (mousePos.x - padx1)*dl_dx + x0;
		 var flux  = (ysize - mousePos.y - pady)*dflux_dy + y0;
		 //https://en.wikipedia.org/wiki/AB_magnitude
		 var Jy    = 3.34e4*(lobs*lobs)*flux*1e-17;
		 var AB = -2.5*Math.log10(Jy/3631.);
		 var lrest = lobs / (znow + 1.);
		 var str = sprintf("(%s,%s,F%s,AB)=(%.2f%s, %.2f%s, %.2f%s, %.2f)", 
				   String.fromCharCode(955,8338), String.fromCharCode(955,7523), String.fromCharCode(955),
				   lobs,String.fromCharCode(8491),lrest,String.fromCharCode(8491),flux, 
				   String.fromCharCode(215)+"10"+String.fromCharCode(8315,185,8311)+" erg cm"+String.fromCharCode(8315,178) +" s"+ String.fromCharCode(8315,185) + " " + String.fromCharCode(8491,8315,185), AB);
//	         ctx.clearRect (xsize-47.*padx2, ysize-0.6*pady, xsize, ysize);
                 ctx.fillStyle = "white";
	         ctx.fillRect (xsize-47.*padx2, ysize-0.6*pady, xsize, ysize);
                 ctx.fillStyle = "black";
		 ctx.fillText(str,  xsize-23.*padx2, ysize-0.2*pady);
	}
    }
    else
    {
	console.log(evt.button);
    }
}, false);

//document.body.addEventListener('mouseup', function (evt){
canvas.addEventListener('mouseup', function (evt){
    if(evt.button === rightclick )
    {
	//record the current cursor position;
	//then set the new x0,x1,y0,y1
	var xsize = canvas.width;
	var ysize = canvas.height; 
	var dx_inv = (xsize-padx)/(x1-x0);
	var dy_inv = (ysize-2.*pady)/(y1-y0);
	var mousePos_new = getMousePos(canvas, evt);
	var min_x = Math.min(mousePos_old.x,mousePos_new.x);
	var max_x = Math.max(mousePos_old.x,mousePos_new.x);
	var min_y = Math.min(mousePos_old.y,mousePos_new.y);
	var max_y = Math.max(mousePos_old.y,mousePos_new.y);
	
	prev_x0 = x0;
	prev_x1 = x1;
	prev_y0 = y0;
	prev_y1 = y1;
	x0 = prev_x0 + (min_x - padx1)/dx_inv; 
	x1 = prev_x0 + (max_x - padx1)/dx_inv; 
	y0 = prev_y0 + (ysize - pady - max_y)/dy_inv; 
	y1 = prev_y0 + (ysize - pady - min_y)/dy_inv; 
	plotData();
    }

}, false);

//add the used line names/wavelengths to the line list table
function writeLineListTable(linelist){
    var nrows = 1;
    var table = document.getElementById("tableLinelist");
    // Create an empty <tr> element and add it to the 1st position of the table:
    for(var lineindex=0; lineindex < linelist.length; lineindex++)
    {
	//add a table row;
	var row = table.insertRow(nrows);
	var cell0 = row.insertCell(0);
	var cell1 = row.insertCell(1);
	var cell2 = row.insertCell(2);
	var cell3 = row.insertCell(3);
	var str_ref = "none";
	//write the name, the lambda, the em/abs flag, the reference
	cell0.innerHTML = linelist[lineindex].name;
	cell1.innerHTML = linelist[lineindex].lambda;
	cell2.innerHTML = linelist[lineindex].emmabs;
	switch (linelist[lineindex].ref)
	{
	    case "msw"      : str_ref = "<a href='http://www.star.ucl.ac.uk/~msw/lines.html'>Mark Westmoquette's compilation</a>"; break;
	    case "B12"      : str_ref = "<a href='http://arxiv.org/abs/1207.7326v1'>Bolton et al., 2012</a>"; break;
	    case "spZ"      : str_ref = "<a href='http://data.sdss3.org/datamodel/files/BOSS_SPECTRO_REDUX/RUN2D/PLATE4/RUN1D/spZline.html'>SDSS spZline data model (DR12)</a>"; break;
	    case "Charlton" : str_ref = "<a href='http://ned.ipac.caltech.edu/level5/Charlton/Charlton1_1.html'>Charlton & Churchill (2000)</a>"; break;
	    case "NIST"     : str_ref = "<a href='http://physics.nist.gov/PhysRefData/ASD/lines_form.html'>NIST  (then converted to vacuum wavelengths)</a>"; break;
	    case "NIST_Fe"  : str_ref = "<a href='http://physics.nist.gov/PhysRefData/Handbook/Tables/irontable2.htm'>NIST Iron lines table</a>"; break;
	    case "VB01"     : str_ref = "<a href='http://adsabs.harvard.edu/abs/2001AJ....122..549V'>Vanden Berk et al. (2001)</a>"; break;
	}
	cell3.innerHTML = str_ref;
	nrows ++;
    }
}

function downloadPng(){
    // var plate = parseInt(document.getElementById("pipeParams_PLATE").innerHTML);
    // var mjd = parseInt(document.getElementById("pipeParams_MJD").innerHTML);
    // var fiberid = parseInt(document.getElementById("pipeParams_FIBERID").innerHTML);
    // var str_plate = (plate < 10000 ? ("0000" + plate).slice(-4) : ("00000" + plate).slice(-5));
    //document.getElementById("downloader").download = "spec-" + str_plate + "-" + mjd + "-" + ("0000" + fiberid).slice(-4) + ".png";
    document.getElementById("downloader").download = specfile_slug + ".png";
    document.getElementById("downloader").href = document.getElementById("myCanvas").toDataURL("image/png").replace(/^data:image\/[^;]/, 'data:application/octet-stream');
}

// from https://github.com/stevebest/julian
function convertJDtoDate(jd) {
    const DAY_IN_MS = 86400000;
    const UNIX_EPOCH_JULIAN_DATE = 2440587.5;
    return new Date((Number(jd) - UNIX_EPOCH_JULIAN_DATE) * DAY_IN_MS);
};

function convertMJDtoDate(mjd) {
    const DAY_IN_MS = 86400000;
    const UNIX_EPOCH_JULIAN_DATE = 2440587.5;
    const MJD_TO_JD = 2400000.5;
    return new Date((Number(mjd) + MJD_TO_JD - UNIX_EPOCH_JULIAN_DATE) * DAY_IN_MS);
};

function zoomStamps(inout){
    // if (inout == "in") {
    //     stamp_numpix = stamp_numpix * 0.8;
    // } else if (inout == "out") {
    //     stamp_numpix = stamp_numpix * 1.25;
    // }
    if (inout == "in") {
        stamp_scale = stamp_scale * 0.8;
    } else if (inout == "out") {
        stamp_scale = stamp_scale * 1.25;
    } else if (inout == "reset") {
        stamp_scale = stamp_scale_default;
    }
    setupStamps(keyvals[keynames.indexOf("PLUG_RA")],
                keyvals[keynames.indexOf("PLUG_DEC")]);
}

function setupStamps(ra, dec){
    var _inner_format = "<a href='${url_link}' target='_blank'><img title='${title}' src='${url_img}' height='160px' width='160px' alt='go to viewer'></a>";
    var _url_img_format = 'http://www.legacysurvey.org/viewer/jpeg-cutout/?ra=${ra}&dec=${dec}&pixscale=${scale}&layer=${layer}&size=${numpix}';
    var _url_link_format = 'http://www.legacysurvey.org/viewer?ra=${ra}&dec=${dec}&layer=${layer}&mark=${ra},${dec}&zoom=18';

    var el = document.getElementById("thumb_span_zoom");
    el.innerHTML = "Image size:<br>" + (Math.floor(stamp_numpix)*stamp_scale).toFixed(1) + " arcsec";
    
    var layers = [//"galex",
                  "sdss",
                  "ls-dr9",
                  "hsc2",
                  "unwise-neo6",
                  "vlass"];
    for(var l=0;l<layers.length;l++){
        var layer = layers[l];
        var td = document.getElementById("thumb_panel_" + layer);
        var _url_img = _url_img_format.replace(/\${ra}/g, ra);
        _url_img = _url_img.replace(/\${dec}/g, dec);
        _url_img = _url_img.replace(/\${layer}/g, layer);
        _url_img = _url_img.replace(/\${scale}/g, stamp_scale);
        _url_img = _url_img.replace(/\${numpix}/g, Math.floor(stamp_numpix));
        
        var _url_link = _url_link_format.replace(/\${ra}/g, ra);
        _url_link = _url_link.replace(/\${dec}/g, dec);
        _url_link = _url_link.replace(/\${layer}/g, layer);
        
        var _inner = _inner_format.replace(/\${title}/g, 'Cutout: ' + layer);
        _inner = _inner.replace(/\${url_img}/g, _url_img);
        _inner = _inner.replace(/\${url_link}/g, _url_link);
        td.innerHTML = _inner;
    }



    // now use the hips2fits service

    // http://alasky.u-strasbg.fr/hips-image-services/hips2fits
    // https://aladin.u-strasbg.fr/hips/list

    var _url_img_format = 'https://alasky.u-strasbg.fr/hips-image-services/hips2fits?hips=${layer}&width=${numpix}&height=${numpix}&fov=${size_deg}&projection=TAN&ra=${ra}&dec=${dec}&format=jpg';
    var _url_link_format =  'http://aladin.unistra.fr/AladinLite/?target=${ra}${dec}&fov=0.05&survey=${layer}';

    var layers = [// {'name': 'xmm', 'hips': 'ESAVO/P/XMM/EPIC-RGB'},
                  // {'name': 'csc', 'hips': 'cxc.harvard.edu/P/cda/hips/allsky/rgb'},
                  {'name': '2mass', 'hips': 'CDS/P/2MASS/color'},
                  {'name': 'vikingk', 'hips' : 'CDS/P/VISTA/VIKING/K'},
                  {'name': 'dss2', 'hips': 'CDS/P/DSS2/color'},
                  // {'name': 'nvss', 'hips': 'CDS/P/NVSS'},
                  {'name': 'ps1', 'hips': 'CDS/P/PanSTARRS/DR1/color-i-r-g'},
     	          {'name': 'galex', 'hips': 'CDS/P/GALEXGR6/AIS/color'},
                  {'name': 'sdssu', 'hips': 'CDS/P/SDSS9/u'} ];
    for(var l=0; l<layers.length; l++){
        var layer = layers[l];
        var td = document.getElementById("thumb_panel_" + layer['name']);
        var _url_img = _url_img_format.replace(/\${ra}/g, Number(ra).toFixed(7));
        var size_deg = Math.floor(stamp_numpix) * stamp_scale / 3600.0;
        _url_img = _url_img.replace(/\${dec}/g, Number(dec).toFixed(7));
        _url_img = _url_img.replace(/\${layer}/g, layer['hips']);
        _url_img = _url_img.replace(/\${size_deg}/g, size_deg);
        _url_img = _url_img.replace(/\${numpix}/g, Math.floor(stamp_numpix));

        var _url_link = _url_link_format.replace(/\${ra}/g, Number(ra).toFixed(7));
        var dec_signed = (dec >= 0.0 ? '+'  : '' ) + Number(dec).toFixed(7); 
        _url_link = _url_link.replace(/\${dec}/g, dec_signed);
        _url_link = _url_link.replace(/\${layer}/g, layer['hips']);

        var _inner = _inner_format.replace(/\${title}/g, 'Cutout: ' + layer);
        _inner = _inner.replace(/\${url_img}/g, _url_img);
        _inner = _inner.replace(/\${url_link}/g, _url_link);
        td.innerHTML = _inner;

    }
    
}
